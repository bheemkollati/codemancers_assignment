import React from 'react'

function Pagination({currentPage,itemPerPage,totalItems,pageSelected}) {
    console.log(totalItems);
    const pageNumbers = [];

    for(let i =1; i<= Math.ceil(totalItems/itemPerPage); i++) {
        pageNumbers.push(i);
    }

    return <nav>
        <ul className="pagination pagination-sm justify-content-end border-0">
            {pageNumbers.map(number => {
                //to know what page we are on
                let classes = "page-item"
                if(number===currentPage)
                {
                    classes += "active"
                }

                return (
                    <li className={classes} key={number}>
                        <a onClick={()=> pageSelected(number)} href="#" className="page-link">{number}</a>
                    </li>
                )
            })}
        </ul>
    </nav>
}

export default Pagination
